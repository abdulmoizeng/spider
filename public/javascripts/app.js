/**
 * Created by abdulmoiz on 10/6/2016.
 */
var app = angular.module("spiderApp", []);
app.controller("spiderCtrl", function($scope, $http) {
    $scope.validateUrl = function(url){
        if(!url || (url.indexOf('http://') == -1 && url.indexOf('https://') == -1)){
            $scope.hasError = true;
            return false;
        }
        return true;
    };
    $scope.crawlUrl = function(url){
      if($scope.validateUrl(url)){
          $http.post('/api/crawler', {url: url}).then(function(data){
              console.log('Success!!');
              console.log(data);
              $scope.showFileContent = true;
              $scope.filePath = data.data.filePath;
              $scope.fileContent = data.data.data;
              //data.data.data
          }, function(err){
              alert('Error while crawling on URL: '+url);
          });
      }
    }
    $scope.clearData = function(){
        $scope.url="";
        $scope.showFileContent = false;
        $scope.fileContent = '';
        $scope.filePath = '';
    }
});
