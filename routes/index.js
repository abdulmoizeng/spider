var express = require('express');
var router = express.Router();
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/api/crawler', function(req, res){
  var urlToCrawl = req.body.url;
  request(urlToCrawl, function(error, response, body) {
    if(error || response.statusCode !== 200) {
      res.status(400);
      return res.json({
        message: 'Bad Url to crawl!',
        error: error
      });
    }
    // Parse the document body
    var $ = cheerio.load(body);
    var fileContent = "Page title:  " + $('title').text()+ "\n";
    fileContent +="URL FOUND ARE : "+ "\n\n";
    $('a').each(function(index,element){
      if(!element.attribs.href || !element.attribs.href.startsWith('http'))return ;//don't save any invalid url
      fileContent+= "Link: "+element.attribs.href+" \t";
      if(element.children && element.children[0] && element.children[0].data){
        fileContent+="Text: "+element.children[0].data;
      }
      fileContent+="\n";
    });
    var filePath = "files/url"+Date.now()+".txt";
    var stream = fs.createWriteStream(filePath);
    stream.once('open', function(fd) {
      stream.write(fileContent);
      stream.end();
      res.status(200);
      res.json({
        message: 'Success!',
        data: fileContent,
        filePath:filePath
      });
    });


  });

});

module.exports = router;
